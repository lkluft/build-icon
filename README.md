# Reproducible ICON builds

Clone, configure and build an ICON binary in a **reproducible way**.
The full process is configured using a single YAML file.

```sh
./build_icon.py -f config.yaml
```
