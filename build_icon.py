#!/usr/bin/env python3
import argparse
import os
import subprocess

import yaml


def parse_config(config_file):
    with open(config_file, "r") as fp:
        conf = yaml.safe_load(fp)

    return conf


def clone_repo(repo, path):
    subprocess.run(
        [
            "git",
            "clone",
            "--recursive",
            "--depth=1",
            repo,
            path,
        ]
    )


def checkout(revision):
    subprocess.run(
        [
            "git",
            "fetch",
            "--depth=1",
            "origin",
            revision,
        ]
    )

    subprocess.run(
        [
            "git",
            "checkout",
            revision,
        ]
    )


def configure_build(config_wrapper, config_flags):
    subprocess.run(
        [
            config_wrapper,
            *config_flags,
        ]
    )


def build_icon(make_flags):
    subprocess.run(
        [
            "make",
            *make_flags,
        ]
    )


def parse_args():
    parser = argparse.ArgumentParser(
        prog="build_icon",
        description="Build ICON in a reproducible way.",
    )
    parser.add_argument(
        "-f", "--file", default="config.yaml", help="YAML file with build configuration"
    )
    return parser.parse_args()


def _main():
    args = parse_args()
    conf = parse_config(args.file)

    clone_repo(conf["repo"], conf["path"])

    os.chdir(conf["path"])

    checkout(conf["revision"])

    configure_build(conf["config_wrapper"], conf["config_options"])
    build_icon(conf["make_options"])


if __name__ == "__main__":
    _main()
